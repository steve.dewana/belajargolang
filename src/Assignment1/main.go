package main

import (
	"fmt"
	"os"
	"strconv"
)

type dataDiri struct {
	nama	string
	age		int
	job		string
}

func main() {

	//inputan := os.Args[0:]
	datastaff := []dataDiri{
	{nama : "Steve", age : 17, job : "QA"},
	{nama : "Jaka", age : 18, job : "Dev"},
	{nama : "Edwin", age : 19, job : "QA - Manager"},
	{nama : "Eka", age : 20, job : "Tech Lead"},
	{nama : "Toni", age : 17, job : "Dev"} }

	var b []*dataDiri

	for i ,_ := range datastaff{
		b = append(b, &datastaff[i])		
	}

	/*
	printDatastaff := func (points []*dataDiri){
		for x, eachDatastaff := range points {
			fmt.Println(x+1, eachDatastaff.nama, eachDatastaff.age, eachDatastaff.job)
		}		
	}
	
	printDatastaff(b)
	*/


	inputan, err := strconv.Atoi(os.Args[1])
	if err != nil {
		// ... handle error
        panic(err)
    }

    fmt.Println(datastaff[inputan].nama, datastaff[inputan].age, datastaff[inputan].job)
}