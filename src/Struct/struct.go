package main

import (
	"fmt"
)

type dataDiri struct {
	nama	string
	age		int
	city	string
}

func main() {

	datastaff := []dataDiri{
	{nama : "Steve", age : 17, city : "Semarang"},
	{nama : "Jaka", age : 18, city : "Jakarta"},
	{nama : "Edwin", age : 19, city : "Yogyakarta"} }

	var b []*dataDiri

	for i ,_ := range datastaff{
		b = append(b, &datastaff[i])		
	}

	printDatastaff := func (points []*dataDiri){
		for x, eachDatastaff := range points {
			fmt.Println(x+1, eachDatastaff.nama, eachDatastaff.age, eachDatastaff.city)
		}		
	}
	
	printDatastaff(b)
}