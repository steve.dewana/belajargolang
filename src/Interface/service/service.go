package service

import "fmt"

type User struct {
	Nama string
}

type Age struct {
	AgeUser int
}

var RegisteredNameList []User

var RegisteredAgeList []Age

type UserService struct {
}

type UserIface interface {
	Register(u *User, um *Age)
	GetUser()
}

func NewUserService() UserIface {
	return &UserService{}
}

func (u *UserService) Register(user *User, umur *Age) {
	RegisteredNameList = append(RegisteredNameList, *user)
	RegisteredAgeList = append(RegisteredAgeList, *umur)
}

func (u *UserService) GetUser() {
	for key, value := range RegisteredNameList {
		fmt.Println(key+1, "nama", value, "Age", RegisteredAgeList[key])
	}

}
