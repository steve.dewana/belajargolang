package main

import (

	"gitlab.com/steve.dewana/belajargolang/-/tree/main/src/Interface/service"
)

func main() {
	//var db []*service.User
	userSvc := service.NewUserService()
	userSvc.Register(&service.User{Nama: "Budi"}, &service.Age{AgeUser: 12})	
	userSvc.Register(&service.User{Nama: "Agus"}, &service.Age{AgeUser: 13})
	userSvc.Register(&service.User{Nama: "Tono"}, &service.Age{AgeUser: 11})	
	userSvc.Register(&service.User{Nama: "Joko"}, &service.Age{AgeUser: 14})
	userSvc.Register(&service.User{Nama: "Tino"}, &service.Age{AgeUser: 12})
	userSvc.GetUser()
}