package main

import "fmt"

func main() {
    
	names := []string{"Edwin","Hugo","Bintang","Jaka","Hans","Toni","Stevanus","Rizky","Billy","Guntur"}

	cetak(names)

}

func cetak(names []string) {
	for _, nama := range names {
		fmt.Println(nama)
	}
}