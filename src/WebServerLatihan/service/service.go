package service

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type User struct {
	Nama string `json:"nama"`
	Kota string `json:"kota"`
}

type dataUser struct {
	db []*User
}

type UserInterface interface {
	RegisterProses(w http.ResponseWriter, r *http.Request)
	GetUserProses(w http.ResponseWriter, r *http.Request)
	Register(u *User) string
	GetUser() []*User
}

func NewUserService(db []*User) UserInterface {
	return &dataUser{
		db: db,
	}
}

func (u *dataUser) Register(user *User) string {
	u.db = append(u.db, user)
	//return user.Nama + " " + user.Kota + "berhasil didaftarkan"
	return user.Nama + "berhasil didaftarkan"
}

func (u *dataUser) GetUser() []*User {
	return u.db
}

func (u *dataUser) RegisterProses(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		decoder := json.NewDecoder(r.Body)
		var user User
		err := decoder.Decode(&user)
		if err != nil {
			fmt.Println("error data user")
			return
		}
		u.Register(&user)
		//w.Write([]byte(user.Nama + " " + user.Kota + " berhasil didaftarkan"))
		w.Write([]byte(user.Nama + " berhasil didaftarkan"))
	} else {
		w.Write([]byte("invalid http method"))
	}
}

func (u *dataUser) GetUserProses(w http.ResponseWriter, r *http.Request) {
	users := u.GetUser()
	data, _ := json.Marshal(users)
	w.Header().Add("content-type", "application/json")
	w.Write(data)
}