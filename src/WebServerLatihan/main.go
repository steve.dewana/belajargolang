package main

import (
	"net/http"

	"gitlab.com/steve.dewana/belajargolang/-/tree/main/src/WebServerLatihan/service"
)

func main() {
	var db []*service.User
	userSvc := service.NewUserService(db)
	http.HandleFunc("/register", userSvc.RegisterProses)
	http.HandleFunc("/user", userSvc.GetUserProses)
	http.ListenAndServe("localhost:8080", nil)
}